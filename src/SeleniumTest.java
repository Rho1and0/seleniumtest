import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.Augmenter;

import java.io.File;
import java.io.IOException;


public class SeleniumTest {

	public static void main(String[] args) {
		//String exePath = ".\\drivers\\chromedriver.exe";
		String exePath = ".\\drivers\\geckodriver.exe";
		//System.setProperty("webdriver.chrome.driver", exePath);
		System.setProperty("webdriver.gecko.driver", exePath);
		WebDriver driver = new FirefoxDriver();

		driver.get("http://www.compra.bo");

		driver.findElement(By.className("link-login")).click();

		WebElement email = driver.findElement(By.id("edit-mail"));
		WebElement pass = driver.findElement(By.id("edit-passwd"));

		email.sendKeys("email-account");
		pass.sendKeys("password");

		driver.findElement(By.id("edit-submit")).click();

		driver.findElement(By.className("menu-login-register")).findElement(By.className("link-name-customer")).click();
		driver.findElement(By.className("menu-login-register")).findElement(By.className("first")).click();

		WebDriver augmentedDriver = new Augmenter().augment(driver);
		File screenshotFile = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);

		try {
			FileUtils.copyFile(screenshotFile, new File("output/success/isloggin.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		driver.close();
	}
}
